defmodule Amorphos do
  @moduledoc """
  Documentation for Amorphos.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Amorphos.hello
      :world

  """
  def hello do
    :world
  end
end
